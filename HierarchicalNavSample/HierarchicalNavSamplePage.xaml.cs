﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchicalNavSample
{
    public partial class HierarchicalNavSamplePage : ContentPage
    {
        public HierarchicalNavSamplePage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(HierarchicalNavSamplePage)}:  ctor");
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        void OnEternalYouthTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnEternalYouthTapped)}");
            Navigation.PushAsync(new EternalYouthPage());
        }

        async void OnDirectionsTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDirectionsTapped)}");

            string response = await DisplayActionSheet("Overloaded Action Sheet",
                              "Cancel",
                              null,
                              "Pugs",
                              "Kittens",
                              "Gorillas",
                              "Sloth");
            Debug.WriteLine($"User picked:  {response}");

            if (response.Equals("pugs", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new HappyvillePage("123 Main St, San Diego, CA"));
            }
        }

        async void OnLunchTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnLunchTapped)}");

            bool usersResponse = await DisplayAlert("Free Lunch Rulez!",
                         "Are you really sure you want this yummy, fantastic, delicious, totally not a trap free lunch??",
                         "Bring it on!",
                         "Nah");

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new FreeLunchPage());
            }

        }
    }
}

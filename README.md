# README #

### 2018.02.07
In class tonight, we:
* Added an example of passing data from view to view in hierarchical navigation.
* Added an example of using DisplayAlert.
* Added an example of using DisplayActionSheet.

### 2018.02.05
This is a cleaned up version of the code we wrote in class on 2018.02.05, demonstrating hierarchical navigation in Xamarin.Forms.